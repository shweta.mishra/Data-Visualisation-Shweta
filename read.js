var fs = require('fs');
fs.readFile('ipl/matches.csv','utf8',function (err,data){
  if(err){
    console.log(err);
    return err;
  }
 var matches = splitData(data);
 var season = fetchData(matches);
 var seasonDataArray = objectToArray(season);
/*final value for matches over year got in seasonDataArray*/
 var mId = matchId(matches);
  fs.readFile('ipl/deliveries.csv', 'utf8',function(err,data){
    if(err){
      console.log(err);
      return err;
    }
    deliveries = splitData(data);
   /*final value got in exRun,now grouping all solutions*/
    var exRun = extraRuns(deliveries,mId);

  /*final value for bowler economy*/
    var bowler = bowlerData(deliveries,matches);

  /*final value for matches win per season for each team in finalSeriesData*/
    var series = seriesData(matches);
    var finalSeriesData = setSeriesData(series,season);

    var finalValue = {sol1 : seasonDataArray, sol2 : exRun, sol3 : bowler,sol4:finalSeriesData};
    fs.writeFile('public/data.json',JSON.stringify(finalValue),(err)=>{
      if(err)
        throw err;
      console.log('file has been saved');
    });
  });
});

/*for spliting data n csv file*/
function splitData(data){
  var  matches = data.split('\n');
  matches.pop();
  matches = matches.map(function splitColum(col){
    return  col.split(',');
  });
  return matches;
}

/*function for fetching data from 2-d array to object*/
function fetchData(matches){
  var season = {};
  matches.shift();
  matches.forEach(function(data){
    if(!season[data[1]])
      {season[data[1]] = 1;
    }
    else{
      season[data[1]]+= 1;
    }
  });
  return season;
}

/*converting season object to array*/
function objectToArray(season){
  var seasonDataArray = [];
  Object.keys(season).map(function(key){
    seasonDataArray.push([Number(key),season[key]]);
  });
  return seasonDataArray;
}


/*3rd problem statement*/
/*finding distinct matchId in 2016*/

function matchId(matches){
  var mId = [];
  matches.forEach(function(year){
    if((year[1] == 2016)&&((mId.indexOf(year[0])) == -1)){
      mId.push(year[0]);
    }
  });
  return mId;
}
/*finding teams and their extra runs based on their match Id's in 2016*/
function extraRuns(deliveries,mId){
  var bowlerTeam = {};
  var exRun = [];
  deliveries.shift();
  mId.map(function(tmOne){
    deliveries.map(function(tmTwo){
      if ((Number(tmOne)) === Number(tmTwo[0]) && (!(bowlerTeam[tmTwo[3]]))){
        bowlerTeam[tmTwo[3]] = Number(tmTwo[16]);
      }
      else if((bowlerTeam[tmTwo[3]]) && ((Number(tmOne)) === Number(tmTwo[0])))
        bowlerTeam[tmTwo[3]]+= Number(tmTwo[16]);
    });
  });
  /*converting into required dataSet format*/
  Object.keys(bowlerTeam).map(function(key){
    exRun.push([(key) , bowlerTeam[key]]);
  });
  return exRun;
}

/*problem statement 4*/

function bowlerData(deliveries,matches){
  var data = {};
  var finalData = [];
  var mId = [];
  matches.forEach(function(year){
    if((year[1] == 2015) && ((mId.indexOf(year[0])) == -1)){
      mId.push(year[0]);
    }
  });

  mId.map(function(el){
    deliveries.map(function(val){
      if((el == val[0]) && (!data[val[8]])){
       data[val[8]] = [Number(val[17]),1];
      }
      else if((el == val[0]) && (data[val[8]])){
        data[val[8]] = [data[val[8]][0]+= Number(val[17]) , data[val[8]][1]+= 1];
      }  });
  });
  /*calculating economy rate of each bowler*/
  for(key in data){
    data[key] = Number((((data[key][0])/(data[key][1]))*6).toFixed(2));
  }
  /*converting into required dataSet format*/
  Object.keys(data).map(function(key){
    if(data[key]<7)
    finalData.push([key , data[key]]);
  });
  return finalData;
}


/*problem statement 2*/
function seriesData(matches){
 var series = [];
 matches.forEach(function (row){
  var year = row[1];
  var flag = 0;
  /*cheking if array has no object,then push directly*/
  if(series.length === 0){
    var ob = {};
      ob['name'] = row[10];
    ob[year] = 1;
    series.push(ob);
      }
/*else condition start to check in object inside array*/
  else{
    /*for checking if any object in the array has name as teamname */
    for(i=0;i<series.length;i++){
      if(series[i].name == row[10]){
        flag = 1;
        /*particular year win data is there then increase count*/
        if(series[i][year]){
          series[i][year]+= 1;
        }
        /*else add the year in object*/
        else{
          series[i][year]= 1;
        }
        break;
      }
    }
      /*if the name is not there push a new object of that team*/
    if(flag === 0) {
      var ob = {};
        ob['name']=row[10];
      ob[year]=1;
      series.push(ob);
    }
  }
});
  return series;
}

/*converting series data into stack bar chart dataSet format*/
function setSeriesData(series,season){
  var years = [];
  /*find all sasons of ipl*/
  Object.keys(season).forEach(function(key){
    years.push(key);
  });
/*map the data into required data set format*/
  series.map(function(object){
    var data = [];
    years.forEach(function(year){
      if(object[year])
        data.push(object[year])
      else
        data.push(0);
    });

    object['data'] = data;
    Object.keys(object).forEach(function(key){
      if((key != 'name')&&(key != 'data')){
        delete object[key];
      }
    })
  })
return{x:years , ser:series};
}
