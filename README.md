This file contains info about all the files used in plotting the graph.

'read.js' : consist of code to read the given data and do manipulations to make it generate required
            data set for plotting a graph.

'index.html' : consist of html code for designing the layout of the page for plotting all the graphs.

'data.json' : contains the data sets for plotting the graph.

'charts.js' : contains code for plotting graph.
