$(document).ready(function(){
  $.ajax({type:"GET",url:"data.json",success:function(res){

    Highcharts.chart('container1', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Matches played per year of all the years of IPL'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Matches (in number)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Matches played in 2008: <b>{point.y}</b>'
      },
      series: [{
        name: 'Population',
        data: res['sol1'],
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }]
      });

    Highcharts.chart('container2', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Exceeded runs per team in year 2016'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Excedded runs (in number)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Excedded runs: <b>{point.y}</b>'
      },
      series: [{
        name: 'Population',
        data: res['sol2'],
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }]
      });


    Highcharts.chart('container3', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Economy rate of bowler in 2015'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Matches (in number)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Economy rate of bowler in 2015: <b>{point.y}</b>'
      },
      series: [{
        name: 'Population',
        data: res['sol3'],
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }]
      });


    Highcharts.chart('container4', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Number of Matches won by each team per year'
      },
      xAxis: {
        categories: res['sol4']['x']
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total no. of match wins'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
        }
      },
      legend: {
        align: 'right',
        x: -10,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
          }
        }
      },
      series: res['sol4']['ser']
    });

  }
})});
